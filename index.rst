.. Siesta Manual documentation master file, created by
   sphinx-quickstart on Thu Jul 20 12:09:19 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Siesta Reference
================

.. toctree::
   :maxdepth: 1

   siesta/index.rst
   utilities/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

INTRODUCTION
============


(Spanish Initiative for Electronic Simulations with Thousands of Atoms)
is both a method and its computer program implementation, to perform
electronic structure calculations and *ab initio* molecular dynamics
simulations of molecules and solids. Its main characteristics are:

-  It uses the standard Kohn-Sham selfconsistent density functional
   method in the local density (LDA-LSD) and generalized gradient (GGA)
   approximations, as well as in a non local functional that includes
   van der Waals interactions (VDW-DF).

-  It uses norm-conserving pseudopotentials in their fully nonlocal
   (Kleinman-Bylander) form.

-  It uses atomic orbitals as a basis set, allowing unlimited
   multiple-zeta and angular momenta, polarization and off-site
   orbitals. The radial shape of every orbital is numerical and any
   shape can be used and provided by the user, with the only condition
   that it has to be of finite support, i.e., it has to be strictly zero
   beyond a user-provided distance from the corresponding nucleus.
   Finite-support basis sets are the key for calculating the Hamiltonian
   and overlap matrices in :math:`O(N)` operations.

-  Projects the electron wavefunctions and density onto a real-space
   grid in order to calculate the Hartree and exchange-correlation
   potentials and their matrix elements.

-  Besides the standard Rayleigh-Ritz eigenstate method, it allows the
   use of localized linear combinations of the occupied orbitals
   (valence-bond or Wannier-like functions), making the computer time
   and memory scale linearly with the number of atoms. Simulations with
   several hundred atoms are feasible with modest workstations.

-  It is written in Fortran 95 and memory is allocated dynamically.

-  It may be compiled for serial or parallel execution (under MPI).

It routinely provides:

-  Total and partial energies.

-  Atomic forces.

-  Stress tensor.

-  Electric dipole moment.

-  Atomic, orbital and bond populations (Mulliken).

-  Electron density.

And also (though not all options are compatible):

-  Geometry relaxation, fixed or variable cell.

-  Constant-temperature molecular dynamics (Nose thermostat).

-  Variable cell dynamics (Parrinello-Rahman).

-  Spin polarized calculations (colinear or not).

-  k-sampling of the Brillouin zone.

-  Local and orbital-projected density of states.

-  COOP and COHP curves for chemical bonding analysis.

-  Dielectric polarization.

-  Vibrations (phonons).

-  Band structure.

-  Ballistic electron transport under non-equilibrium (through )

Starting from version 3.0,  includes the module.  provides the ability
to model open-boundary systems where ballistic electron transport is
taking place. Using  one can compute electronic transport properties,
such as the zero bias conductance and the I-V characteristic, of a
nanoscale system in contact with two electrodes at different
electrochemical potentials. The method is based on using non equilibrium
Greens functions (NEGF), that are constructed using the density
functional theory Hamiltonian obtained from a given electron density. A
new density is computed using the NEGF formalism, which closes the
DFT-NEGF self consistent cycle.

Starting from version 4.1,  is an intrinsic part of the  code. I.e. a
separate executable is not necessary anymore. See
Sec. `11 <#sec:transiesta>`__ for details.

For more details on the formalism, see the main  reference cited below.
A section has been added to this User’s Guide, that describes the
necessary steps involved in doing transport calculations, together with
the currently implemented input options.

**References:**

-  “Unconstrained minimization approach for electronic computations that
   scales linearly with system size” P. Ordejón, D. A. Drabold, M. P.
   Grumbach and R. M. Martin, Phys. Rev. B **48**, 14646 (1993); “Linear
   system-size methods for electronic-structure calculations” Phys. Rev.
   B **51** 1456 (1995), and references therein.

   Description of the order-*N* eigensolvers implemented in this code.

-  “Self-consistent order-:math:`N` density-functional calculations for
   very large systems” P. Ordejón, E. Artacho and J. M. Soler, Phys.
   Rev. B **53**, 10441, (1996).

   Description of a previous version of this methodology.

-  “Density functional method for very large systems with LCAO basis
   sets” D. Sánchez-Portal, P. Ordejón, E. Artacho and J. M. Soler, Int.
   J. Quantum Chem., **65**, 453 (1997).

   Description of the present method and code.

-  “Linear-scaling ab-initio calculations for large and complex systems”
   E. Artacho, D. Sánchez-Portal, P. Ordejón, A. Garcı́a and J. M. Soler,
   Phys. Stat. Sol. (b) **215**, 809 (1999).

   Description of the numerical atomic orbitals (NAOs) most commonly
   used in the code, and brief review of applications as of March 1999.

-  “Numerical atomic orbitals for linear-scaling calculations” J.
   Junquera, O. Paz, D. Sánchez-Portal, and E. Artacho, Phys. Rev. B
   **64**, 235111, (2001).

   Improved, soft-confined NAOs.

-  “The  method for ab initio order-:math:`N` materials simulation” J.
   M. Soler, E. Artacho, J.D. Gale, A. Garcı́a, J. Junquera, P. Ordejón,
   and D. Sánchez-Portal, J. Phys.: Condens. Matter **14**, 2745-2779
   (2002)

   Extensive description of the  method.

-  “Computing the properties of materials from first principles with”,
   D. Sánchez-Portal, P. Ordejón, and E. Canadell, Structure and Bonding
   **113**, 103-170 (2004).

   Extensive review of applications as of summer 2003.

-  “Improvements on non-equilibrium and transport Green function
   techniques: The next-generation TranSIESTA”, Nick Papior, Nicolas
   Lorente, Thomas Frederiksen, Alberto García and Mads Brandbyge,
   Computer Physics Communications, **212**, 8–24 (2017).
   :doi:`10.1016/j.cpc.2016.09.022`

   Description of the  method.

-  “Density-functional method for nonequilibrium electron transport”,
   Mads Brandbyge, Jose-Luis Mozos, Pablo Ordejón, Jeremy Taylor, and
   Kurt Stokbro, Phys. Rev. B **65**, 165401 (2002).

   Description of the original  method (prior to 4.1).

For more information you can visit the web page
http://siesta-project.org


.. Siesta Manual pieces


Siesta Manual
=============

This Reference Manual contains descriptions of all the input, output
and execution features of SIESTA, but is not really a tutorial introduction to
the program. Interested users can find tutorial material prepared for
 schools and workshops at the project’s web page
http://siesta-project.org

.. toctree::
   :maxdepth: 1
   :hidden:	      
   :caption: Contents:

   introduction.rst
   compilation.md
   descriptors.rst
   test.rst
   

   

Tests
=====

.. math:: e^{i\pi} + 1 = 0
   :label: euler

Euler's identity, equation :math:numref:`euler`, was elected one of the
most beautiful mathematical formulas.


.. describe:: PAPER

   You can set this variable to select a paper size.

       
.. object:: SCF.MD.Tolerance [1.e-4] (dimensionless)

   :index:`SCF.MD.Tolerance <single: FDF; Tol>`
   You can set this variable to select a paper size.

.. object::  MeshCutoff [100.0] (Ry)

   :index:`SCF.MD.Tolerance <pair: mesh; Cutoff>`
   Mesh cutoff
   
   
.. glossary::

   term 1 : A
   term 2 : B
      Definition of both terms.

      

General system descriptors
==========================

.. describe:: SystemLabel [string] <siesta>

A *single* word (max. 20 characters *without blanks*) containing a
nickname of the system, used to name output files.

.. describe:: SystemName [string]

A string of one or several words containing a descriptive name of the
system (max. 150 characters).

.. describe:: NumberOfSpecies [integer] <>

Number of different atomic species in the simulation. Atoms of the same
species, but with a different pseudopotential or basis set are counted
as different species.

This is not required to be set.

.. describe:: NumberOfAtoms[integer]<>

Number of atoms in the simulation.

This is not required to be set.

.. describe:: ChemicalSpeciesLabel[block]

It specifies the different chemical species that are present, assigning
them a number for further identification.  recognizes the different
atoms by the given atomic number::

     %block ChemicalSpecieslabel
        1   6   C    pbe/C.psml
        2  14   Si
        3  14   Si_surface  Si.psf
     %endblock ChemicalSpecieslabel

The first number in a line is the species number, it is followed by the
atomic number, and then by the desired label. This label will be used to
identify corresponding files, namely, pseudopotential file, user basis
file, basis output file, and local pseudopotential output file.

This construction allows you to have atoms of the same species but with
different basis or pseudopotential, for example.

Negative atomic numbers are used for *ghost* atoms (see ).

For atomic numbers over :math:`200` or below :math:`-200` you should
read .

This block is mandatory.

.. describe:: SyntheticAtoms [block]

This block is an additional block to complement for special atomic
numbers.

Atomic numbers over :math:`200` are used to represent *synthetic atoms*
(created for example as a “mixture” of two real ones for a “virtual
crystal” (VCA) calculation). In this special case a new block must be
present to give  information about the “ground state” of the synthetic
atom::

  1 201 ON-0.50000 1 # Species index
  2 2 3 4 # n numbers for valence states with l=0,1,2,3
  2.0 3.5 0.0 0.0 # occupations of valence states with l=0,1,2,3

Pseudopotentials for synthetic atoms can be created using the and
programs in the directory.

Atomic numbers below :math:`-200` represent *ghost synthetic atoms*.

.. describe:: AtomicMass[block]

It allows the user to introduce the atomic masses of the different
species used in the calculation, useful for the dynamics with isotopes,
for example. If a species index is not found within the block, the
natural mass for the corresponding atomic number is assumed. If the
block is absent all masses are the natural ones. One line per species
with the species index (integer) and the desired mass (real). The order
is not important. If there is no integer and/or no real numbers within
the line, the line is disregarded::

   3  21.5
   1   3.2

The default atomic mass are the natural masses. For *ghost* atoms (i.e.
floating orbitals) the mass is :math:`10^{30}` au.
